 **Table of content:**
 - [Systems Biology Markup Language, SBML](#SBML)
 - [SBML-associated software]("SBML-soft")
 - [Simulation Experiment Description Markup Language, SED-ML]("SED-ML")
 - [Systems Biology Graphical Notation, SBGN]("SBGN")
 - [Systems Biology Graphical Notation Markup Language, SBGN-ML]("SBGN-ML")
 - [Synthetic Biology Open Language Visual, SBOLv]("SBOLv")
 - [Pharmacometrics Markup Language, PhamML]("PharmML")
 - [COMBINE archive]("OMEX")

<a id="SBML"></a>
# Systems Biology Markup Language, SBML

SBML is a software data format for describing computational models used in Systems Biology. SBML is modular, with a common core augmented by "packages" covering different types of models. I was elected editor three times, 2006–2008, 2012–2014, and 2016–2018. In addition to the core, I participated particularly to the packages _multi_ (rule-based models), _qual_ (logic models), and _distrib_ (distributions and uncertainty).

## Where to find it?

https://sbml.org/

## Cite 

Here are the main papers to cite, the initial ones presenting the language, plus the latest I co-authored on the different parts.

Hucka M., Bolouri H., Finney A., Sauro H.M., Doyle J.C., Kitano H., and the rest of the Systems Biology Software Developer's Forum: Arkin A.P., Bornstein B.J., Bray D., Cornish-Bowden A., Cuellar A.A., Dronov S., Ginkel M., Gor V., Goryanin I.I., Hedley W.J., Hodgman T.C., Hunter P.J., Juty N.S., Kasberger J.L., Kremling A., Kummer U., Le Novère N., Loew L.M., Lucio D., Mendes P., Mjolsness E.D., Nakayama Y., Nelson M.R., Nielsen P.F., Sakurada T., Schaff J.C., Shapiro B.E., Shimizu T.S., Spence H.D., Stelling J., Takahashi K., Tomita M., Wagner J., Wang J. The Systems Biology Markup Language (SBML): A Medium for Representation and Exchange of Biochemical Network Models. _Bioinformatics_ (2003), 19: 524-531. [doi:10.1093/bioinformatics/btg015](https://doi.org/10.1093/bioinformatics/btg015)

Hucka M., Bergmann F.T., Chouiya C., Dräger A., Hoops S., Keating S.M., König M., Le Novère N., Myers C.J., Olivier B.G., Sahle S., Schaff J.C., Smith L.P., Waltemath D., Wilkinson D.J., Zhang F. The Systems Biology Markup Language (SBML): Language Specification for Level 3 Version 2 Core Release 2. _Journal of Integrative Bioinformatics_ (2019) 16(2) 20190021. [doi:10.1515/jib-2019-0021](https://doi.org/10.1515/jib-2019-0021)

Chaouiya C., Berenguier D., Keating S.M., Naldi A., van Iersel M.P., Rodriguez N., Dräger A., Büchel F., Cokelaer T., Kowal B., Wicks B., Gonçalves E., Dorier J., Page M., Monteiro P.T., von Kamp A., Xenarios I., de Jong H., Hucka M., Klamt S., Thieffry D., Le Novère N., Saez-Rodriguez J., Helikar T. SBML Qualitative Models: a model representation format and infrastructure to foster interactions between qualitative modelling formalisms and tools. _BMC Systems Biology_ (2013) 7:135. [doi:10.1186/1752-0509-7-135](https://doi.org/10.1186/1752-0509-7-135)

Chaouiya C., Keating S.M., Berenguier D., Naldi A., Thieffry D., van Iersel M.P., Le Novère N., Helikar T. The Systems Biology Markup Language (SBML) Level 3 Package: Qualitative Models, Version 1, Release 1. _Journal of Integrative Bioinformatics_ (2015) 12(2): 270. [doi:10.1515/jib-2015-270](https://doi.org/10.1515/jib-2015-270)

<a id="SBML-soft"></a>
# SBML-associated software

I was involved in the development of a few associated software tools, amongst which:

## JSBML
A community-driven project to create a free, open-source (under LGPL), pure Java™ library for reading, writing, and manipulating SBML files and data streams. 

### Where to find it?
https://sbml.org/software/jsbml/

### Cite 
Dräger A., Rodriguez N., Dumousseau M., Dörr A., Wrzodek C., Le Novère N., Zell A., Hucka M. JSBML: a flexible Java library for working with SBML. _Bioinformatics_ (2011), 27: 2167-2168. [doi:10.1093/bioinformatics/btr361](https://doi.org/10.1093/bioinformatics/btr361)

Rodriguez N., Thomas A., Watanabe L., Vazirabad I.Y., Kofia V., Gómez H.F., Mittag F., Matthes J., Rudolph J., Wrzodek F., Netz E., Diamantikos A., Eichner J., Keller R., Wrzodek C., Fröhlich S., Lewis N.E., Myers C.J., Le Novère N., Palsson B.Ø., Hucka M., Dräger A. JSBML 1.0: providing a smorgasbord of options to encode systems biology models. _Bioinformatics_ (2015) 31(20):3383-3366. [doi:10.1093/bioinformatics/btv341](https://doi.org/10.1093/bioinformatics/btv341)

## Systems Biology Format Converter (SBFC)

SBFC is both a framework and an online service for converting between formats used in systems biology. The framework is written in Java and can be used as a standalone executable or as web service. The SBFC framework currently supports conversion from SBML to BioPAX Levels 2 and 3, MATLAB, Octave, XPP, GraphViz, and APM.

### Where to find it?
https://sbml.org/software/sbfc/

### Cite 
Rodriguez N., Pettit J.B., Dalle Pezze P., Li L., Henry A., van Iersel M.P., Jalowicki G., Kutmon M., Natarajan K. N., Tolnay D., Stefan M.I., Evelo C.T., Le Novère N. The Systems Biology Format Converter. _BMC Bioinformatics_ (2016) 17(1):154. [doi:10.1186/s12859-016-1000-2](https://doi.org/10.1186/s12859-016-1000-2)

## SBMLeditor

SBMLeditor was a portable, low-level, tree-structured, Java editor for SBML, supporting annotations and validation.

### Where to find it?
SBMLeditor is no longer maintained

### Cite
Rodriguez N, Donizelli M, Le Novère N. SBMLeditor: effective creation of models in the Systems Biology Markup Language (SBML).
_BMC Bioinformatics_ (2007), 8:79. [doi:10.1186/1471-2105-8-79](https://doi.org/10.1186/1471-2105-8-79)

<a id="SED-ML"></a>
# Simulation Experiment Description Markup Language, SED-ML
SED-ML is an XML-based format for encoding simulation setups, to ensure exchangeability and reproducibility of simulation experiments. 

## Where to find it?
https://sed-ml.org/

## Cite

SED-ML was first presented in:

Köhn D, Le Novère N. SED-ML – An XML Format for the Implementation of the MIASE Guidelines. _Lecture Notes in Computer Science_ (2008), Computational Methods in Systems Biology. [doi:10.1007/978-3-540-88562-7_15](https://doi.org/10.1007/978-3-540-88562-7_15) 

but the main publication is:

Waltemath D, Adams R, Bergmann FT, Hucka M, Kolpakov F, Miller AK, Moraru II, Nickerson D, Sahle S, Snoep JL, Le Novère N. Reproducible computational biology experiments with SED-ML--the Simulation Experiment Description Markup Language. _BMC Syst Biol_ (2011) 5:198.
[doi:10.1186/1752-0509-5-198](https://doi.org/10.1186/1752-0509-5-198)

<a id="SBGN"></a>
# Systems Biology Graphical Notation, SBGN
SBGN is a standard graphical representation intended to foster the efficient storage, exchange and reuse of information about signaling pathways, metabolic networks, and gene regulatory networks amongst communities of biochemists, biologists, and theoreticians. SBGN is made up of three complementary languages: Process descriptions, Activity flows, and Entity relationships.

## Where to find it?
https://sbgn.github.io/

## Cite

Le Novère N., Hucka M., Mi H., Moodie S., Shreiber F., Sorokin A., Demir E., Wegner K., Aladjem M., Wimalaratne S., Bergman F.T., Gauges R., Ghazal P., Kawaji H., Li L., Matsuoka Y., Villéger A., Boyd S.E., Calzone L., Courtot M., Dogrusoz U., Freeman T., Funahashi A., Ghosh S., Jouraku A., Kim S., Kolpakov F., Luna A., Sahle S., Schmidt E., Watterson S., Goryanin I., Kell D.B., Sander C., Sauro H., Snoep J.L., Kohn K., Kitano H. The Systems Biology Graphical Notation. _Nature Biotechnology_ (2009) 27:735-741. [doi:10.1038/nbt.1558](https://doi.org/10.1038/nbt.1558)

<a id="SBGN-ML"></a>
# libSBGN and SBGN-ML
LibSBGN is a software support library for the Systems Biology Graphical Notation (SBGN). SBGN-ML is an associated standard format to exchange maps encoded in SBGN.

## Where to find it
https://github.com/sbgn/libsbgn

## Cite
van Iersel MP, Villéger AC, Czauderna T, Boyd SE, Bergmann FT, Luna A, Demir E, Sorokin A, Dogrusoz U, Matsuoka Y, Funahashi A, Aladjem MI, Mi H, Moodie SL, Kitano H, Le Novère N, Schreiber F. Software support for SBGN maps: SBGN-ML and LibSBGN. _Bioinformatics_ (2012) , 28(15):2016-2021. [doi:10.1093/bioinformatics/bts270](https://doi.org/10.1093/bioinformatics/bts270)

<a id="SBOLv"></a>
# Synthetic Biology Open Language Visual, SBOLv
SBOL Visual is a guideline to graphically depict genetic designs and their functional interactions. 

## Where to find it?
https://sbolstandard.org/visual-about/

## Cite
Quinn J.Y., Cox R.S. 3rd, Adler A., Beal J., Bhatia S., Cai Y., Chen J., Clancy K., Galdzicki M., Hillson N.J., Le Novère N., Maheshwari A.J., McLaughlin J.A., Myers C.J., P U., Pocock M., Rodriguez C., Soldatova L., Stan G.B., Swainston N., Wipat A., Sauro H.M. SBOL Visual: A Graphical Language for Genetic Designs. _PLoS Biology_ (2015) 13(12):e1002310. [doi:10.1371/journal.pbio.1002310](https://doi.org/10.1371/journal.pbio.1002310)

Baig H., Fontanarossa P., Kulkarni V., McLaughlin J., Vaidyanathan P., Bartley B., Bhakta S., Bhatia S., Bissel M., Clancy K., Cox R.S., Moreno. A.G., Gorochowski T., Grünberg R., Lee J., Luna A., Madsen C., Mısırlı G., Nguyen T., Le Novère N., Palchick Z., Pocock M., Roehner N., Sauro H., Scott-Brown J., Sexton J.T., Stan G.-B., Tabor J.J., Vilar Vazquez M., Voigt C.A., Wipat A., Zong D., Zundel Z., Beal J., Myers C. Synthetic Biology Open Language Visual (SBOL Visual) Version 2.3. _Journal of Integrative Bioinformatics_ (2021) 18(3): 20200045. [doi:10.1515/jib-2020-0045](https://doi.org/10.1515/jib-2020-0045)

<a id="PhamML"></a>
# Pharmacometrics Markup Language, PharmML
PharmML an exchange standard in pharmacometrics, providing means to encode models, trial designs, and modeling steps.

# Where to find it?
https://github.com/pharmml

## Cite
Swat M.J., Moodie S., Wimalaratne S.M., Kristensen N.R., Lavielle M., Mari A., Magni P., Bizzotto R., Pasotti L, Mezzalana E, Smith M. K., Comets E., Sarr C., Terranova N., Blaudez E., Chan P., Chard J., Chatel K., Chenel M., Edwards D., Franklin C, Giorgino T., Glont M., Girard P., Grenon P., Harling K., Hooker A.C, Kaye R., Keizer R., Kloft C., Kok, J. N., Kokash N., Laibe C., Laveille C., Lestini G., Mentré F., Mezzalana E., Munafo A., Nordgren R., Nyberg H.B., Para-Guillen Z.P., Pasotti L., Plan E., Ribba B., Smith G., Trocóniz I.F., Yvon F., Harnisch L., Karlsson M., Hermjakob H., Le Novère N. Pharmacometrics Markup Language (PharmML): Opening New Perspectives for Model Exchange in Drug Development. _CPT: Pharmacometrics & Systems Pharmacology_ (2015) 6:316-319. [doi:10.1002/psp4.57](https://doi.org/10.1002/psp4.57) 

<a id="OMEX"></a>
# COMBINE archive
A COMBINE archive is a single file containing the various documents, necessary for the description of a model and all associated data and procedures. This includes for instance, but not limited to, simulation experiment descriptions, all models needed to run the simulations and associated data files. The archive is encoded using the Open Modeling EXchange format (OMEX). 

## Where to find it?
http://old_co.mbine.org/standards/omex

# Cite
Bergmann F.T., Adams R., Moodie S., Cooper J., Glont M., Golebiewski M., Hucka M., Laibe C., Miller A.K., Nickerson D.P., Olivier B.G., Rodriguez N., Sauro H.M., Scharm M., Soiland-Reyes S., Waltemath D., Yvon F., Le Novère N. COMBINE archive and OMEX format: one file to share all information to reproduce a modeling project. _BMC Systems Biology_ (2014) 15:369. [doi:10.1186/s12859-014-0369-z](https://doi.org/10.1186/s12859-014-0369-z)
